package fr.univtours.polytech.di.multimedia.primitives;

public class PointOutputScanning extends ForwardFileOutputScanning {
	private int x;
	private int y;
	private int perimetre;
	
	
	public PointOutputScanning(final File file, final int bufferSize, final int x, final int y, final int perimetre) {
		super(file, bufferSize);
		this.x = x;
		this.y = y;
		this.perimetre = perimetre;
	}
	
	
	/**
	 * @see fr.univtours.polytech.di.multimedia.primitives.OutputScanning#writeRecord(fr.univtours.polytech.di.multimedia.primitives.Record)
	 */
	@Override
	public void writeRecord(final Record record) {
		int recordX = Integer.valueOf(record.getField("x"));
		int recordY = Integer.valueOf(record.getField("y"));
		
		double value = Math.sqrt(Math.pow(recordX - x, 2) + Math.pow(recordY - y, 2));
		if (value <= perimetre) {
			super.writeRecord(record);
		}
	}
}
