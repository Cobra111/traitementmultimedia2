package fr.univtours.polytech.di.multimedia.primitives;

import java.io.PrintStream;

public class PairImpairOutputScanning extends OutputScanning {

	/** La position actuelle dans le fichier pair. */
	  private int pairFileIndex;
	  
	  /** La position actuelle dans le fichier impair. */
	  private int impairFileIndex;

	  /** La position de l'enregistrement courant dans le buffer. */
	  private int bufferIndex;

	  /** Le fichier � partir du quel on lit les donn�es. */
	  private final File pairFile;
	  
	  /** Le fichier � partir du quel on lit les donn�es. */
	  private final File impairFile;

	  /** Le buffer. */
	  private final Buffer buffer;

	  /**
	   * Le constructeur
	   * @param file le fichier � partir duquel les donn�es sont lues
	   * @param bufferSize la taille du buffer d'�crire en nombre d'enregistrements
	   */
	  public PairImpairOutputScanning(final File pairFile, final File impairFile, final int bufferSize) {
	    pairFileIndex = 0;
	    impairFileIndex = 0;
	    bufferIndex = 0;
	    this.pairFile = pairFile;
	    this.impairFile = impairFile;
	    buffer = new Buffer(bufferSize);
	    declareBuffer(buffer);
	  }

	  /**
	   * @see fr.univtours.polytech.di.multimedia.primitives.Drawable#displayStatistics()
	   */
	  @Override
	  protected void displayStatistics() {
	    super.displayStatistics();
	    pairFile.displayStatistics();
	    impairFile.displayStatistics();
	  }

	  /**
	   * @see fr.univtours.polytech.di.multimedia.primitives.OutputScanning#flushRecords()
	   */
	  @Override
	  public void flushRecords() {
		  for (int i = 0; i < buffer.getValidRecordCount(); i++) {
			  Record record = buffer.getRecord(i);
			  if (record.getNumericKey() % 2 == 0) {
				  pairFile.writeRecord(pairFileIndex, record);
				  pairFileIndex++;
			  }
			  else {
				  impairFile.writeRecord(impairFileIndex, record);
				  impairFileIndex++;
			  }
		  }
		  
		  buffer.invalidateAllRecords();
		  bufferIndex = 0;
	  }

	  /**
	   * @see fr.univtours.polytech.di.multimedia.primitives.Algorithm#generateGraphRepresentation(java.io.PrintStream)
	   */
	  @Override
	  protected String generateGraphRepresentation(final PrintStream out) {
	    final String myNodeId = getNodeId();

	    final String impairFileId = impairFile.generateGraphRepresentation(out);
	    final String pairFileId = pairFile.generateGraphRepresentation(out);

	    super.generateGraphRepresentation(out);

	    out.println("\tsubgraph cluster_" + myNodeId + " { fillcolor=chocolate2 }");
	    out.println("\tbuffer0_" + myNodeId + " -> " + impairFileId + "\n");
	    out.println("\tbuffer0_" + myNodeId + " -> " + pairFileId + "\n");

	    return "buffer0_" + myNodeId;
	  }

	  /**
	   * @see fr.univtours.polytech.di.multimedia.primitives.OutputScanning#writeRecord(fr.univtours.polytech.di.multimedia.primitives.Record)
	   */
	  @Override
	  public void writeRecord(final Record record) {
	    buffer.setRecord(bufferIndex, record);
	    bufferIndex++;
	    if (bufferIndex == buffer.getSize()) {
	      flushRecords();
	    }
	  }

}
