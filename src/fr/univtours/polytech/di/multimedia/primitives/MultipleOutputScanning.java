package fr.univtours.polytech.di.multimedia.primitives;

import java.util.List;

public class MultipleOutputScanning extends OutputScanning {
	private List<OutputScanning> outputScannings;
	
	public MultipleOutputScanning(List<OutputScanning> outputScannings) {
		this.outputScannings = outputScannings;
		for (int i = 0; i < outputScannings.size(); i++) {
			declareOutputDrawable(outputScannings.get(i));
		}
	}

	@Override
	public void flushRecords() {
		for (int i = 0; i < outputScannings.size(); i++) {
			outputScannings.get(i).flushRecords();
		}
	}

	@Override
	public void writeRecord(Record record) {
		for (int i = 0; i < outputScannings.size(); i++) {
			outputScannings.get(i).writeRecord(record);
		}
	}

}
