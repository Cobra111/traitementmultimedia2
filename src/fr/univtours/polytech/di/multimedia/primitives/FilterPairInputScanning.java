package fr.univtours.polytech.di.multimedia.primitives;

import java.io.PrintStream;

/**
 * Classe permettant de lire les enregistrements d'un fichier dans l'ordre en
 * utilisant un buffer pour minimiser le nombre d'entr�es/sorties.
 * @author S�bastien Aupetit
 */
public class FilterPairInputScanning extends InputScanning {

  /** La position actuelle dans le fichier. */
  private int fileIndex;

  /** La position de l'enregistrement courant dans le buffer. */
  private int bufferIndex;

  /** Le fichier � partir du quel on lit les donn�es. */
  private final File file;

  /** Le buffer. */
  private final Buffer buffer;

  /**
   * Le constructeur.
   * @param file le fichier � partir duquel les donn�es sont lues.
   * @param bufferSize la taille du buffer de lecture en nombre
   *          d'enregistrements
   */
  public FilterPairInputScanning(final File file, final int bufferSize) {
    fileIndex = 0;
    this.file = file;
    buffer = new Buffer(bufferSize);
    declareBuffer(buffer);
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.primitives.Drawable#displayStatistics()
   */
  @Override
  protected void displayStatistics() {
    super.displayStatistics();
    file.displayStatistics();
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.primitives.Algorithm#generateGraphRepresentation(java.io.PrintStream)
   */
  @Override
  protected String generateGraphRepresentation(final PrintStream out) {
    final String myNodeId = getNodeId();

    final String fileId = file.generateGraphRepresentation(out);

    super.generateGraphRepresentation(out);

    out.println("\tsubgraph cluster_" + myNodeId + " { fillcolor=coral2 }");
    out.println("\t" + fileId + " -> " + "buffer0_" + myNodeId + "\n");

    return "buffer0_" + myNodeId;
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.primitives.InputScanning#getFirstAvailableRecord(fr.univtours.polytech.di.multimedia.primitives.Record)
   */
  @Override
  public boolean getFirstAvailableRecord(final Record record) {
    if (buffer.getValidRecordCount() == 0) {
      if (!readBuffer()) {
        return false;
      }
    }
    if(buffer.getRecord(bufferIndex).getNumericKey() % 2 == 0) {
    	record.copy(buffer.getRecord(bufferIndex));
    }
    else {
    	markFirstAvailableRecordAsRead();
    	getFirstAvailableRecord(record);
    }
    return true;
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.primitives.InputScanning#markFirstAvailableRecordAsRead()
   */
  @Override
  public void markFirstAvailableRecordAsRead() {
    buffer.invalidateRecord(bufferIndex);
    bufferIndex++;
    getFirstAvailableRecord(new Record());
  }

  /**
   * Effectue la lecture d'un buffer � partir du fichier.
   * @return true si au moins un enregistrement a pu �tre lu.
   */
  private boolean readBuffer() {
    final int count = file.readLeftAlignedBuffer(fileIndex, buffer);
    if (count == 0) {
      return false;
    } else {
      bufferIndex = 0;
      fileIndex += count;
      return true;
    }
  }
}
