package fr.univtours.polytech.di.multimedia.primitives;

import java.util.List;


public class MergeOrderedInputs extends InputScanning {
	private int indexInput;
	private List<InputScanning> inputScannings;

	public MergeOrderedInputs(List<InputScanning> inputScannings) { 
		indexInput = -1;
		this.inputScannings = inputScannings;
		for (int i = 0; i < inputScannings.size(); i++) {
			declareInputDrawable(inputScannings.get(i));
		}
	}
	
	
	@Override
	public boolean getFirstAvailableRecord(Record record) {
		if (inputScannings.size() == 0) {
			return false;
		}
		
		Record tempRecord = new Record();
		for (int i = 0; i < inputScannings.size(); i++) {
			InputScanning inputScanning = inputScannings.get(i);
			if (inputScanning.getFirstAvailableRecord(tempRecord)== false) {
				inputScannings.remove(i);
				return getFirstAvailableRecord(record);
			}
			if ((i == 0) || (tempRecord.getNumericKey() < record.getNumericKey())){
				record.copy(tempRecord);
				indexInput = i;
			}
		}
		
		return true;
	}

	@Override
	public void markFirstAvailableRecordAsRead() {
		inputScannings.get(indexInput).markFirstAvailableRecordAsRead();
	}

}
