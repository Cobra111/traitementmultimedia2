package fr.univtours.polytech.di.multimedia.primitives;

public class PairImpairProcessor extends Processor {

	  /** The input reader. */
	  protected InputScanning inputReader;
	
	  /** The pair output writer. */
	  protected OutputScanning pairOutputWriter;
	  
	  /** The impair output writer. */
	  protected OutputScanning impairOutputWriter;
	
	  /**
	   * The Constructor.
	   * @param inputReader le flux d'entrée
	   * @param outputWriter le flux de sortie
	   */
	  public PairImpairProcessor(final InputScanning inputReader,
	      final OutputScanning pairOutputWriter,
	      final OutputScanning impairOutputWriter) {
	    this.inputReader = inputReader;
	    this.impairOutputWriter = impairOutputWriter;
	    this.pairOutputWriter = pairOutputWriter;
	    declareInputDrawable(inputReader);
	    declareOutputDrawable(impairOutputWriter);
	    declareOutputDrawable(pairOutputWriter);
	  }
	
	  /**
	   * @see fr.univtours.polytech.di.multimedia.primitives.Processor#run()
	   */
	  @Override
	  public void run() {
	    final Record record = new Record();
	
	    while (inputReader.getFirstAvailableRecord(record)) {
	    	if (record.getNumericKey() % 2 == 0) {
	    		pairOutputWriter.writeRecord(record);
	    	}
	    	else {
	    		impairOutputWriter.writeRecord(record);
	    	}
	      
	    	inputReader.markFirstAvailableRecordAsRead();
	    }
	    impairOutputWriter.flushRecords();
	    pairOutputWriter.flushRecords();
	  }

}
