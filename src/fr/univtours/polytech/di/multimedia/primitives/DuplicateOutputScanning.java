package fr.univtours.polytech.di.multimedia.primitives;


public class DuplicateOutputScanning extends OutputScanning {
	private FilterImpairOutputScanning impairOutputScanning;
	private FilterPairOutputScanning pairOutputScanning;
	
	public DuplicateOutputScanning(final FilterImpairOutputScanning impairOutputScanning,
			final FilterPairOutputScanning pairOutputScanning) {
		this.impairOutputScanning = impairOutputScanning;
		this.pairOutputScanning = pairOutputScanning;
		declareOutputDrawable(pairOutputScanning);
		declareOutputDrawable(impairOutputScanning);
	}

	@Override
	public void flushRecords() {
		impairOutputScanning.flushRecords();
		pairOutputScanning.flushRecords();
	}

	@Override
	public void writeRecord(Record record) {
		impairOutputScanning.writeRecord(record);
		pairOutputScanning.writeRecord(record);
	}

}
