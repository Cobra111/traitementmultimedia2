package fr.univtours.polytech.di.multimedia.exercices;

import java.util.ArrayList;
import java.util.List;

import fr.univtours.polytech.di.multimedia.primitives.InputScanning;
import fr.univtours.polytech.di.multimedia.primitives.MergeOrderedInputs;
import fr.univtours.polytech.di.multimedia.primitives.MultipleOutputScanning;
import fr.univtours.polytech.di.multimedia.primitives.NetworkBufferReceiver;
import fr.univtours.polytech.di.multimedia.primitives.NetworkBufferSender;
import fr.univtours.polytech.di.multimedia.primitives.OutputScanning;
import fr.univtours.polytech.di.multimedia.primitives.Processor;
import fr.univtours.polytech.di.multimedia.primitives.SimpleProcessor;

/**
 * Exercice 7
 * @author S�bastien Aupetit
 */
public class Exercice7 implements Exercice {
  /** Le nombre de sonde. */
  static final int nbNetworkRecorders = 10;

  /** Le nombre de processur d'analyse. */
  static final int nbAnomalyDetectors = 5;

  /**
   * @see fr.univtours.polytech.di.multimedia.exercices.Exercice#checkOuputFiles()
   */
  @Override
  public boolean checkOuputFiles() {
    // rien � tester
    return true;
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.exercices.Exercice#configureInputFiles()
   */
  @Override
  public void configureInputFiles() {
    // Rien � faire
  }

  /**
   * @see fr.univtours.polytech.di.multimedia.exercices.Exercice#getProcessor()
   */
  @Override
  public Processor getProcessor() {
	  List<InputScanning> recorders = new ArrayList<InputScanning>();
	  List<OutputScanning> detectors = new ArrayList<OutputScanning>();
	  
	  for (int i = 1; i <= nbNetworkRecorders; i++) {
		  recorders.add(new NetworkRecorder(i));
	  }
	  
	  for (int i = 1; i <= nbAnomalyDetectors; i++) {
		  detectors.add(new AnomalyDetector(i));
	  }
	  
	  MergeOrderedInputs inputReader = new MergeOrderedInputs(recorders);
	  
	  NetworkBufferSender sender = new NetworkBufferSender("envoyeur", networkBufferSize, inputReader);
	  final NetworkBufferReceiver receiver = new NetworkBufferReceiver("receveur", sender);
	  final OutputScanning output = new MultipleOutputScanning(detectors);
	  return new SimpleProcessor(receiver, output);
  }

}
